# Cours 10/10/2022
# Timothée BAUDEQUIN
# EPSI
# filename : 10102022_BAUDEQUIN_Timothee_EPSI.py


print("Hello World")

if 5 > 2:
    print("5 est supérieur à 2")

# écrire un commentaire
"""
pour faire des commentaires 
sur plusieurs lignes 
"""

# 2**3 => 2 puissance 3

# opérateur : < > =< >= == !=
# variable != Variable
# pas de maj pour les variables

var_1 = 10
var_2 = 20
#var_1 = var_2
somme = var_1 + var_2
print(somme)

# permutation
print("var1 = " + var_1.__str__())
print("var2 = " + var_2.__str__())
var_1, var_2 = var_2, var_1
print("var1 = " + var_1.__str__())
print("var2 = " + var_2.__str__())

# type de variable
print(type(var_1))
print(type(var_1.__str__()))
print(type(var_1.__float__()))
print(type(True))

# print string
print('Hello')
print("Hello")
print('''Hello''')
print("""Hello""")

# échapper un caractère
# \n retour à la ligne
# \' afficher un ' si le string est ''

# help() exemple = help(print())

# transtypage

var_3 = hex(2022)
var_4 = bin(21)
print(var_3)
print(var_4)


