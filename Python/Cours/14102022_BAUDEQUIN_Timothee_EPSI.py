# Cours 14/10/2022
# Timothée BAUDEQUIN
# EPSI
# file_name = 14102022_BAUDEQUIN_Timothee_EPSI.py
"""
python & ML:
- récursivité
- POO
- paradigme fonctionnel

librairies IA:
AIMA, PyDatalog, Simple IA, GraphLab, PyBrain, NumPy

fonctionnalité de programmation "logique" et prédicats.
Python est adapté pour les bigdata (gros jeu de données)

Haskell, Lisp ou Erlang

exemple de programme:
DeepBlue, AlphaGo, test de Turing (ELIZA, ALICE bot)

Logique floue
Algorithme génétique
Data mining
Réseaux de neuronnes
apprentissage automatique

Data Types:
str (texte)
int, float, complex (numérique)
list, tuple, range (séquences)
dict (mapping)
set, fronzenset (set)
bool (booléan)
bytes, bytearray, memoryview (binary)
NoneType (Type none)

pour afficher le type
print(type(x))

pour spécifier le type
x = int(10)

"""
x = ["pomme", "banane", "cerise"]  # list
y = ("pomme", "banane", "cerise")  # tuple

# print(float(1))
# print(int(2.8))
# print(complex(1))
import random

k = random.randrange(1, 10)

print(k)
###############
a = "Hello World"

print(a[4])
#################
for x in a:
    print(x)
##############
print(len("Lait"))

##########

cherche = "hérisson"
a = "Il était une fois dans un pays"
if cherche in a:
    print("il y a '" + cherche + "' dans la phrase")
else:
    print("il n'y a pas de '" + cherche + "' dans la phrase")

###### Slicing

a = "Hello World"
print(a[6:len(a)])

#####
a = "Hello World"
print(a[-3:-1])


#### afficher la position d'un caractère


def chercher_dans_un_texte(texte, cherche):
    position = 0
    found = False
    for x in texte:
        if x == cherche:
            print("il se trouve à la position n°" + str(position))
            found = True
        elif position == len(texte) - 1 and not found:
            print(cherche + " : n'est pas dans le texte")
        position = position + 1


chercher_dans_un_texte("Hello World", "l")
chercher_dans_un_texte("Hello World", "k")

############ Savoir si c'est un entier ou non
a = 12.9

# les conditions fonctionnent
if a == int(a) or a % 1 < 0:
    print("c'est un entier")
else:
    print("ce n'est pas un entier")

#### cast
a = float(1)
b = float(2.3)
c = float("2")
d = float("4.8")

print(str(a) + " , " + str(b) + " , " + str(c) + " , " + str(d))

#### nomenclature et présentation du code / projet

## tuples python
"""
on ne peut pas modifier un tuple une fois créer  != list 
"""
var_tuple = (2, 3, 4)
# associer plusieurs valeur
a, b = 5, 6
print(a)
print(b)

for x in var_tuple:
    print(x)
print(var_tuple[1])

# une tuple avec une unique valeur
b = (3,)

u = [5]
v, = u
print(v)
print(type(v))

####### Exercice 1
# filename : 14102022_ex1_BAUDEQUIN_Timothee_EPSI.py
prenom = "Pierre"
age = 20
majeur = True
compte_en_banque = 20135.384
amis = ["Marie", "Julien", "Adrien"]
parents = ("Marc", "Caroline")

infos = [prenom, age, majeur, compte_en_banque, amis, parents]

for x in infos:
    print(x)

##### Exercice 2
# filename : 14102022_ex2_BAUDEQUIN_Timothee_EPSI.py

site_web = "google"
print(site_web)

##### Exercice 3
# filename : 14102022_ex3_BAUDEQUIN_Timothee_EPSI.py

nb = 17
print("Le nombre est", nb)
print("Le nombre est " + str(nb))

##### Exercice 4
# filename : 14102022_ex4_BAUDEQUIN_Timothee_EPSI.py

a = 3
b = 6
a = b
b = 7
print(a)  # affiche 6 car prend la valeur de b au moment où on lui assigne.
print(b)  # affiche 7 car c'est la derniere valeur de b

##### Exercice 5
# filename : 14102022_ex5_BAUDEQUIN_Timothee_EPSI.py

a = 2
b = 6
c = 3

print(a, b, c, sep=" + ")  # permet de séparer chaque élément avec ce qui est spécifié

##### Exercice 6
# filename : 14102022_ex6_BAUDEQUIN_Timothee_EPSI.py

list3 = range(3)  # le mot 'list' est un mot de data type
list2 = range(5)
print(list(list2))

##### Exercice 7
# filename : 14102022_ex7_BAUDEQUIN_Timothee_EPSI.py

prenom = "Vincent"

if type(prenom) == str:
    print("Prénom est un string")

prenom = 0
if type(prenom) == int:
    print("Prénom est un nombre entier")

##### Exercice 8
# filename : 14102022_ex8_BAUDEQUIN_Timothee_EPSI.py

## remplacer "salut" dans la phrase
phrase = "salut les dev, salut ça va"
nouvelle_phrase = "Bonsoir" + phrase[5:len(phrase)]
print(nouvelle_phrase)

nouvelle_phrase = phrase.replace("salut", "Coucou", 1)
print(nouvelle_phrase)


### synthaxe pour les projets
"""
Titre 
Nom du projet 
Date de la dernière révision 
Auteur 
IDE 
Client 

#########
programme principal
########
zone d'import 
zone de déclaration des variables globales 
zone de déclaration des modules et fonctions 

"""