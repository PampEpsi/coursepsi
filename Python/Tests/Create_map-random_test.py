import numpy as np
import random


def generatemap(taille, walk):
    map = np.full((taille, taille), 0)

    x = random.randrange(1, taille)
    y = random.randrange(1, taille)
    lastmoove = 0
    moove = 0
    first = True

    for i in range(1, walk):
        while moove == lastmoove or first:
            moove = random.randrange(1, 5)
            first = False
        # debog
        print(i)
        print("moove = ", moove)
        print("lastmoove = ", lastmoove)
        lastmoove = moove
        if moove == 1:
            # up
            map[x,y] = 1
            if y < taille:
                y += 1
        elif moove == 2:
            # left
            map[x, y] = 1
            if x > 0:
                x -= 1
        elif moove == 3:
            # down
            map[x, y] = 1
            if y > 0:
                y -= 1
        elif moove == 4:
            # right
            map[x, y] = 1
            if x < taille:
                x += 1
        else:
            print("else")

    return map


print(generatemap(30, 50))
