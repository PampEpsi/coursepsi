import numpy as np
vecteur_1 = np.zeros(10)
vecteur_2 = np.zeros(10,dtype='int32')
vecteur_3 = np.ones((3,4),dtype='int16')

print("vecteur 1 = ", vecteur_1)
print("vecteur 2 = ", vecteur_2)
print("vecteur 3 = ", vecteur_3)

print("dimension de vecteur 3 = ", vecteur_3.ndim)
print("shape de vecteur 3 = ", vecteur_3.shape)

vecteur_4 = np.full((3,4),5)
vecteur_5 = np.full_like(vecteur_4,10)
vecteur_6 = np.identity(3)

print("vecteur 4 = ", vecteur_4)
print("vecteur 5 = ", vecteur_5)
print("vecteur 6 = ", vecteur_6)