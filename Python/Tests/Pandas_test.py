import pandas as pd
import numpy as np

# construction d'un dataframe

myDataSet = pd.DataFrame(
    {'Col1': [1, 2, 3],
     'Col2': [1.0, 2.0, 3.0],
     'Col3': ['a', 'b', 'c']})


# permet de mettre les colonnes en lignes
myDataSet2 = pd.DataFrame.from_dict(
    {'Col1': [1, 2, 3],
     'Col2': [1.0, 2.0, 3.0],
     'Col3': ['a', 'b', 'c']},
    orient='index'
)

print(myDataSet)
print(myDataSet2)

print("le type des colonnes :\n", myDataSet.dtypes)

# pour transformer le type d'une donnée
myDataSet['Col1'] = myDataSet['Col1'].astype(float)

print("le nouveau type des colonnes :\n", myDataSet.dtypes)

# une autre façon de créer avec numpy
npArray = np.array([[1, 2, 3], [4, 5, 6], [7, 8, 9]])

myDataSet3 = pd.DataFrame(npArray, columns=['Col1', 'Col2', 'Col3'])

print(myDataSet3)

# si on modifie l'array, ça modifie le data set
npArray[0, 0] = 666
print(myDataSet3)

myDataSet4 = pd.read_csv('./Data/ozone.csv')
print(myDataSet4)

myDataSet5 = pd.read_csv('./Data/ozone.txt', delimiter="\t")
print(myDataSet5.head(3))  # limite l'affichage aux 3 premiers

print(myDataSet4.iloc[5])  # ne montre que la 5ème ligne
print(myDataSet4.iloc[5:10])  # montre de 5 à 10

# pour parcourir tout le data set
for index, row in myDataSet4.iterrows():
    print("la  valeur d'indice [{}] :\n{}".format(index, row))

# mettre des filtres
dayOff = myDataSet4.loc[(myDataSet4["JOUR"] == 1) &
                        (myDataSet4['O3obs'] >= 100) &
                        ((myDataSet4["STATION"] == 'Ram') | (myDataSet4["STATION"] == 'Cad'))
                        ]
# permet de reset l'index (#200IQ) drop=True permet de ne pas garder l'ancien index
dayOff = dayOff.reset_index(drop=True)
print(dayOff.head(5))
# pour extraire uniquement les valeurs uniques de la colonne STATION
print(myDataSet4['STATION'].unique())

# mettre un regex en place
result = myDataSet4.loc[myDataSet4['STATION'].str.contains('.a.', regex=True)]
print(result['STATION'].unique())

print(myDataSet4[['TEMPE', 'STATION']].head(5))

# pour print un truc précis
print(myDataSet4.iloc[8, 2])
