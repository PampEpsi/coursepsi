##### Exercice 8
# filename : 14102022_ex8_BAUDEQUIN_Timothee_EPSI.py

## remplacer "salut" dans la phrase
phrase = "salut les dev, salut ça va"
nouvelle_phrase = "Bonsoir" + phrase[5:len(phrase)]
print(nouvelle_phrase)

nouvelle_phrase = phrase.replace("salut", "Coucou", 1)
print(nouvelle_phrase)