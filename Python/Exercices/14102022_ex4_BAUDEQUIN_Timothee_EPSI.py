##### Exercice 4
# filename : 14102022_ex4_BAUDEQUIN_Timothee_EPSI.py

a = 3
b = 6
a = b
b = 7
print(a)  # affiche 6 car prend la valeur de b au moment où on lui assigne.
print(b)  # affiche 7 car c'est la derniere valeur de b