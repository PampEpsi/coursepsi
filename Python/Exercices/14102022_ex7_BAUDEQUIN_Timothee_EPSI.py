##### Exercice 7
# filename : 14102022_ex7_BAUDEQUIN_Timothee_EPSI.py

prenom = "Vincent"

if type(prenom) == str:
    print("Prénom est un string")

prenom = 0
if type(prenom) == int:
    print("Prénom est un nombre entier")